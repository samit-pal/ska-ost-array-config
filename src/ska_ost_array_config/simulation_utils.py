import warnings

import astropy.coordinates
import numpy
from astroplan import Observer, TargetAlwaysUpWarning
from astropy import units
from astropy.time import Time, TimeDelta
from astropy.utils.exceptions import AstropyDeprecationWarning
from scipy.ndimage import rotate
from ska_sdp_datamodels.configuration.config_coordinate_support import (
    ecef_to_enu,
    lla_to_ecef,
)
from ska_sdp_datamodels.image.image_create import create_image
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility
from ska_sdp_func_python.image import fit_psf
from ska_sdp_func_python.imaging import invert_visibility
from ska_sdp_func_python.imaging.weighting import weight_visibility


class ExternalTelescope:
    """
    ExternalTelescope class that provides an interface to non-SKAO telescopes.
    This class is useful if you want to experiment with VLBI experiments involving
    SKAO and non-SKAO telescopes.
    """

    def __init__(self, label, location, station_diameter):
        """
        ExternalTelescope class

        Parameters
        ----------
        label : string
            Name of the telescope
        location : astropy.coordinates.EarthLocation
            Coordinates of the telescope as an EarthLocation object.
        station_diameter : float
            Size of the antenna in metres
        """
        self.label = label
        self.location = location
        self.station_diameter = station_diameter

    def to_enu(self, array_centre):
        """
        Convert the coordinates of the external telescope to ENU coordinates
        with respect to the specified array_centre.

        Parameters
        ----------
        array_centre : astropy.coordinates.EarthLocation

        Returns
        -------
        XYZ coordinates of the external facility in ENU coordinate system with respect
        to the specified array_centre.
        """
        # Convert the coordinates of the external facility in (lon, lat) to ECEF
        x, y, z = lla_to_ecef(
            self.location.lat, self.location.lon, self.location.height.value
        )
        ant_xyz = numpy.array([
            [
                x,
            ],
            [
                y,
            ],
            [
                z,
            ],
        ]).transpose()

        # Convert the ECEF coordinates to ENU coordinates
        warnings.filterwarnings("ignore", category=AstropyDeprecationWarning)
        ant_xyz = ecef_to_enu(array_centre, ant_xyz)
        warnings.resetwarnings()

        return ant_xyz


def simulate_observation(
    array_config,
    phase_centre,
    start_time,
    duration=1.0,
    integration_time=1,
    ref_freq=50e6,
    chan_width=5.4e3,
    n_chan=1000,
    horizon=20,
    freq_undersample=100,
    time_undersample=10,
):
    """Simulate an observation with the SKA telescopes

    Parameters
    ----------
    array_config : ska_sdp_datamodels.configuration.config_model.Configuration
        Array layout
    phase_centre : astropy.coordinates.SkyCoord
        Pointing direction
    start_time : astropy.time.core.Time
        Start time of the observation
    duration : float
        Duration of the observation in seconds. Default: 1 s
    integration_time : float
        Length of each time interval in seconds. Default: 1 s
    ref_freq : float
        Frequency in Hz of the first channel (Default: 50 MHz)
    chan_width : float
        Bandwidth in Hz (Default: 1 MHz)
    n_chan : int
        Number of channels (Default: 1000)
    horizon : float
        Elevation of the horizon in degrees. Default: 20 degrees.
    freq_undersample : int
        Undersampling factor along the frequency dimension.
        Increase this value if your simulation takes too long or
        you are memory limited. Default value of 100 implies only one channel
        is simulated for every 100 channels.
    time_undersample : int
        Undersampling factor along the time dimension.
        Increase this value if your simulation takes too long or
        you are memory limited. Default value of 10 implies only one timeslot
        is simulated for every 10 integration intervals.

    Returns
    -------
    Simulated visibilities (as an instance to
    ska_sdp_datamodels.visibility.vis_model.Visibility)
    """

    assert (
        array_config.names.shape[0] > 1
    ), "Cannot perform interferometric simulations with one station/dish!"

    observatory = Observer(array_config.location)

    # Work out the start and end times of the observation in hour angles
    start_ha = (
        observatory.target_hour_angle(start_time, phase_centre).wrap_at("180d").hour
    )
    end_ha = (
        observatory.target_hour_angle(
            start_time + TimeDelta(duration, format="sec"), phase_centre
        )
        .wrap_at("180d")
        .hour
    )
    time_bins_ha = (numpy.pi / 43200.0) * numpy.arange(
        start_ha * 3600.0,
        end_ha * 3600.0,
        integration_time,
    )[::time_undersample]

    # Frequency setup
    channel_frequencies = numpy.linspace(
        ref_freq,
        ref_freq + (n_chan - 1) * chan_width,
        n_chan,
    )[::freq_undersample]
    channel_width = chan_width * numpy.ones_like(channel_frequencies)

    # Finally, simulate visibilities
    visibility = create_visibility(
        config=array_config,
        times=time_bins_ha,
        times_are_ha=True,
        frequency=channel_frequencies,
        channel_bandwidth=channel_width,
        phasecentre=phase_centre,
        integration_time=integration_time,
        utc_time=start_time,
        polarisation_frame=PolarisationFrame("stokesI"),
        elevation_limit=numpy.deg2rad(horizon),
    )

    return visibility


def find_rise_set_times(location, phase_centre, date, elevation_limit=20.0):
    """
    Find the nearest rise, transit and set times for the specified phase centre

    Parameters
    ----------
    location : astropy.coordinates.EarthLocation
        Location of the observatory
    phase_centre: astropy.coordinates.SkyCoord
        Coordinates of the target
    date: astropy.time.Time
        Reference date to estimate the outputs
    elevation_limit: float
        Minimum elevation in degrees
        Default: 20 degrees

    Returns
    -------
    astropy.time.Time objects corresponding to target rise, transit and set times.
    Rise and set times are set to None if the target is circumpolar. All three elements are
    set to None if the target is never visible from the specified location.
    """
    # Transform location to astroplan.Observer()
    location = Observer(location)
    # Find the next transit time
    transit_time = location.target_meridian_transit_time(
        date, phase_centre, which="next"
    )
    if location.altaz(transit_time, phase_centre).alt.deg < elevation_limit:
        # Target is never above the horizon at this location
        return None, None, None

    # Find the rise and set times associated with this transit time
    warnings.filterwarnings("error")
    try:
        rise_time = location.target_rise_time(
            transit_time,
            phase_centre,
            which="previous",
            horizon=elevation_limit * units.deg,
        )
        set_time = location.target_set_time(
            transit_time,
            phase_centre,
            which="next",
            horizon=elevation_limit * units.deg,
        )
    except TargetAlwaysUpWarning:
        rise_time = None
        set_time = None
    warnings.resetwarnings()

    return rise_time, transit_time, set_time


def generate_mfs_psf(
    visibility,
    cellsize,
    npixel=512,
    weighting="uniform",
    r_value=0.0,
    return_sidelobe_noise=False,
):
    """Weight the visibility data and compute the PSF corresponding to an MFS image
    FIXME: The PSF is computed using a simple 2D transformation.
    FIXME: In the future, we should include W-projection.

    Parameters
    ----------
    visibility : ska_sdp_datamodels.visibility.vis_model.Visibility
        Visibility data
    cellsize : float
        Image plane grid cell size in radians
    npixel : int
        Number of pixels in the image plane
    weighting : string
        Can be 'uniform','natural', or 'robust'.
        If weighting='robust', r_value must be set.
    r_value : float
        Robust value for Briggs weighting scheme.
        Will be discarded for uniform or natural weighting
    return_sidelobe_noise: bool
        If True, the PSF sidelobe noise is returned in addition to the psf.
        The standard deviation of the PSF beyond 1000 arcsec is used as a proxy
        for PSF sidelobe noise. A ValueError is thrown if the specified npixel
        corresponds to a region less than 2000 arcsec.
        Default: False.

    Returns
    -------
    PSF of type ska_sdp_datamodels.image.image_model.Image
    """
    # ska_sdp_datamodels issues a few DeprecationWarning that we cannot fix.
    # Filter them here
    warnings.filterwarnings("ignore", category=DeprecationWarning)

    if weighting not in ["uniform", "natural", "robust"]:
        err_msg = "Unknown visibility weighting scheme"
        raise ValueError(err_msg)
    if return_sidelobe_noise:
        min_pix = numpy.radians(2000.0 / 3600.0) / cellsize
        assert npixel > min_pix, (
            f"To estimate the PSF sidelobe noise, the number of pixels must be greater "
            + "than {min_pix} (for this cellsize)"
        )
    img_template = create_image(
        npixel,
        cellsize,
        visibility.phasecentre,
        polarisation_frame=PolarisationFrame("stokesI"),
        frequency=visibility.frequency.data[0]
        + (visibility.channel_bandwidth[0] * len(visibility.channel_bandwidth) / 2),
        channel_bandwidth=visibility.channel_bandwidth[0]
        * len(visibility.channel_bandwidth),
        nchan=1,
    )
    weighted_vis = weight_visibility(
        visibility, img_template, weighting=weighting, robustness=r_value
    )
    psf, _ = invert_visibility(weighted_vis, img_template, dopsf=True)

    # Reset warning filter set above
    warnings.resetwarnings()

    if return_sidelobe_noise:
        # Mask the inner 1000 arcsec in the PSF image and estimate the standard dev
        pixel_data = numpy.squeeze(psf.pixels.data)
        mask = numpy.zeros_like(pixel_data)
        avoid_size = int(numpy.ceil(numpy.radians(500.0 / 3600.0) / cellsize))
        mask[
            int(psf.refpixel[0]) - avoid_size : int(psf.refpixel[0]) + avoid_size,
            int(psf.refpixel[1]) - avoid_size : int(psf.refpixel[1]) + avoid_size,
        ] = 1
        masked_pixel_data = numpy.ma.masked_array(pixel_data, mask=mask)

        return psf, masked_pixel_data.std()

    return psf


def get_PSF_shape_and_profile(psf, return_profile=False):
    """Fit a 2D Gaussian to the PSF and return the PSF shape.

    Parameters
    ----------
    psf : ska_sdp_datamodels.image.image_model.Image
        PSF object returned by ska_sdp_func_python.imaging.invert_visibility
    return_profile : bool
        If return_profile=True, return the 1D profile of the PSF along the major
        and minor axes.

    Returns
    -------
    A dictionary with bmaj, bmin, and bpa in degrees.
    If return_profile=True, a tulpe of 1D major and minor axes profiles are also returned.
    Both major and minor axes profiles are numpy.ndarrays whose size is the same as the
    number of pixels in the psf image.
    """

    # First, get the orientation of the PSF
    synth_beam = fit_psf(psf)

    if return_profile:
        # Rotate the PSF by BPA degrees
        psf_pixels = numpy.squeeze(psf.pixels.data)
        psf_rotated = rotate(psf_pixels, synth_beam["bpa"], reshape=False)
        # Get the 1D profile along the major and minor axes
        peak = numpy.max(psf_rotated)
        row_idx, col_idx = numpy.where(psf_rotated == peak)
        major_profile = numpy.squeeze(psf_rotated[:, col_idx])
        minor_profile = numpy.squeeze(psf_rotated[row_idx, :])
        return synth_beam, (major_profile, minor_profile)

    return synth_beam
