###########
Change Log
###########

Unreleased
----------
* Add get_mid_dish_coordinates() to ska_ost_array_config.array_config

2.1.0
-----
* Add the subarray templates library (corresponds to memo revision 01)

2.0.0
-----
* Second public release of the staged delivery and array assemblies memo (revision 02).

1.0.5
----------
* Fix bug encountered in 1.0.4 (see OPS-253 for more information)
* Verified to work with py3.10/3.11

1.0.4 (Do not use this version.)
----------
* Update Mid AA2 and MK+ layout following the release of SKA-TEL-SKO-0001827 revision 02.
* Update Low coordinates to include station HAE
* Add get_low_station_rotation() and get_low_station_coordinates() to ska_ost_array_config.array_config

1.0.3
-----
* Ensure proper time-tagging in simulated observations. 

1.0.2
-----
* Verified to work with py3.9/3.10/3.11
* Add plot_collecting_area_vs_bl_length() to SubArray class.

1.0.1
-----
* Update Mid coordinates to version 10.
* Reformat coordinate files to csv

1.0.0
-----
* First public release of the staged delivery and array assemblies memo along with ska_ost_array_config package. 
