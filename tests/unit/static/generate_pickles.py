import datetime
import pickle

from astropy import units
from astropy.coordinates import SkyCoord
from astropy.time import Time

from ska_ost_array_config.array_config import LowSubArray, MidSubArray
from ska_ost_array_config.simulation_utils import simulate_observation

PICKLE_PROTOCOL = 4

# LOW custom type
low = LowSubArray(subarray_type="custom", custom_stations="E1-*,C66")
with open("low_custom.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# LOW AA0.5
low = LowSubArray(subarray_type="AA0.5")
with open("low_aa05.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# LOW AA1
low = LowSubArray(subarray_type="AA1")
with open("low_aa1.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# LOW AA2
low = LowSubArray(subarray_type="AA2")
with open("low_aa2.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# LOW AA*
low = LowSubArray(subarray_type="AA*")
with open("low_aastar.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# LOW AA4
low = LowSubArray(subarray_type="AA4")
with open("low_aa4.pickle", "wb") as f:
    pickle.dump(low, f, protocol=PICKLE_PROTOCOL)

# MID custom type
mid = MidSubArray(subarray_type="custom", custom_stations="M*,SKA001")
with open("mid_custom.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID AA0.5
mid = MidSubArray(subarray_type="AA0.5")
with open("mid_aa05.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID AA1
mid = MidSubArray(subarray_type="AA1")
with open("mid_aa1.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID AA2
mid = MidSubArray(subarray_type="AA2")
with open("mid_aa2.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID AA*
mid = MidSubArray(subarray_type="AA*")
with open("mid_aastar.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID AA4
mid = MidSubArray(subarray_type="AA4")
with open("mid_aa4.pickle", "wb") as f:
    pickle.dump(mid, f, protocol=PICKLE_PROTOCOL)

# MID Observation
phase_centre = SkyCoord("04:00:00 -80:00:00", unit=(units.hourangle, units.deg))
ref_time = Time(datetime.datetime(2023, 5, 21, 19))
observation = simulate_observation(
    array_config=MidSubArray(subarray_type="AA*").array_config,
    phase_centre=phase_centre,
    start_time=ref_time,
    duration=360.0,
    integration_time=1,
    ref_freq=1420e6,
    chan_width=13.4e3,
    n_chan=200,
    horizon=20,
    freq_undersample=100,
    time_undersample=10,
)
with open("mid_observation.pickle", "wb") as f:
    pickle.dump(observation, f, protocol=PICKLE_PROTOCOL)
