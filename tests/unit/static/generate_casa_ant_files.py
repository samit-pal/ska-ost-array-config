from ska_ost_array_config.array_config import LowSubArray, MidSubArray

low_custom = LowSubArray(subarray_type="custom", custom_stations="C*,E1-*")
low_custom.generate_casa_antenna_list("low_reference.casa.cfg")

mid_custom = MidSubArray(subarray_type="custom", custom_stations="M*,SKA017")
mid_custom.generate_casa_antenna_list("mid_reference.casa.cfg")
