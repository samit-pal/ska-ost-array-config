import pickle
from pathlib import Path

from matplotlib.testing.compare import compare_images

from ska_ost_array_config import UVW

from .test_simulation_utils import TOLERANCE


def test_uvw_without_autocorr(test_observation):
    """Test UVW.UVW(ignore_autocorr=True)"""
    pickle_file = (
        Path(__file__).resolve().parent.parent / "static/mid_observation.pickle"
    )
    with open(pickle_file, "rb") as f:
        expected_obs = pickle.load(f)
    uvw = UVW.UVW(test_observation, ignore_autocorr=True)
    expected_uvw = UVW.UVW(expected_obs, ignore_autocorr=True)
    assert (uvw.u_m - expected_uvw.u_m < TOLERANCE).all()
    assert (uvw.v_m - expected_uvw.v_m < TOLERANCE).all()
    assert (uvw.u_wave - expected_uvw.u_wave < TOLERANCE).all()
    assert (uvw.v_wave - expected_uvw.v_wave < TOLERANCE).all()
    assert (uvw.uvdist_m - expected_uvw.uvdist_m < TOLERANCE).all()
    assert (uvw.uvdist_lambda - expected_uvw.uvdist_lambda < TOLERANCE).all()


def test_uvw_with_autocorr(test_observation):
    """Test UVW.UVW(ignore_autocorr=False)"""
    pickle_file = (
        Path(__file__).resolve().parent.parent / "static/mid_observation.pickle"
    )
    with open(pickle_file, "rb") as f:
        expected_obs = pickle.load(f)
    uvw = UVW.UVW(test_observation, ignore_autocorr=False)
    expected_uvw = UVW.UVW(expected_obs, ignore_autocorr=False)
    assert (uvw.u_m - expected_uvw.u_m < TOLERANCE).all()
    assert (uvw.v_m - expected_uvw.v_m < TOLERANCE).all()
    assert (uvw.u_wave - expected_uvw.u_wave < TOLERANCE).all()
    assert (uvw.v_wave - expected_uvw.v_wave < TOLERANCE).all()
    assert (uvw.uvdist_m - expected_uvw.uvdist_m < TOLERANCE).all()
    assert (uvw.uvdist_lambda - expected_uvw.uvdist_lambda < TOLERANCE).all()


def test_plot_uv_coverage(test_observation, test_image_name):
    """Test UVW.plot_uv_coverage()"""
    uvw = UVW.UVW(test_observation, ignore_autocorr=True)

    # Test scale="kilo", method="lambda"
    fig, axes = UVW.plot_uv_coverage(uvw)
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_plot_klambda.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None

    # Test scale="", method="lambda"
    fig, axes = UVW.plot_uv_coverage(uvw, scale="")
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_plot_lambda.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None

    # Test scale="kilo", method="meter"
    fig, axes = UVW.plot_uv_coverage(uvw, method="meter", scale="kilo")
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_plot_kmeter.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None

    # Test scale="", method="meter"
    fig, axes = UVW.plot_uv_coverage(uvw, method="meter", scale="")
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_plot_meter.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None


def test_plot_baseline_distribution(test_observation, test_image_name):
    """Test UVW.plot_baseline_distribution()"""
    uvw = UVW.UVW(test_observation, ignore_autocorr=True)

    # Test scale="kilo", method="lambda"
    fig, axes = UVW.plot_baseline_distribution(
        uvw, method="lambda", scale="kilo", plot_type="uv"
    )
    UVW.plot_baseline_distribution(
        uvw, method="lambda", scale="kilo", plot_type="u", axes=axes
    )
    UVW.plot_baseline_distribution(
        uvw, method="lambda", scale="kilo", plot_type="v", axes=axes
    )
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_hist_klambda.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None

    # Test scale="", method="metre"
    fig, axes = UVW.plot_baseline_distribution(
        uvw, method="metre", scale="", plot_type="uv"
    )
    UVW.plot_baseline_distribution(
        uvw, method="metre", scale="", plot_type="u", axes=axes
    )
    UVW.plot_baseline_distribution(
        uvw, method="metre", scale="", plot_type="v", axes=axes
    )
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent
        / "static/mid_reference_uv_hist_metre.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None
