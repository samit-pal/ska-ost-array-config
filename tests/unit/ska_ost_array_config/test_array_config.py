import filecmp
import pickle
import warnings
from pathlib import Path

import numpy
import pytest
from astropy import units
from astropy.coordinates import EarthLocation
from matplotlib.testing.compare import compare_images

from ska_ost_array_config.array_config import (
    LowSubArray,
    MidSubArray,
    SubarrayTemplateLibrary,
    filter_array_by_distance,
    get_low_station_coordinates,
    get_low_station_rotation,
    get_mid_dish_coordinates,
)
from ska_ost_array_config.simulation_utils import ExternalTelescope

from .test_simulation_utils import TOLERANCE


def test_get_low_station_rotation():
    """Test if get_low_station_rotation returns valid rotation angle"""
    assert get_low_station_rotation("C1") - 0.0 < TOLERANCE
    assert get_low_station_rotation("N16-5") - 144.5 < TOLERANCE

    with pytest.raises(RuntimeError):
        get_low_station_rotation("B0")


def test_get_low_station_coordinates():
    """Test if get_low_station_coordinates returns valid coordinates"""
    test_coords = get_low_station_coordinates("C1")
    assert (test_coords.x.value - (-2565115.544092137)) < TOLERANCE
    assert (test_coords.y.value - (5085629.234035365)) < TOLERANCE
    assert (test_coords.z.value - (-2861196.7160952426)) < TOLERANCE

    with pytest.raises(RuntimeError):
        get_low_station_coordinates("B0")


def test_get_mid_dish_coordinates():
    """Test if get_mid_dish_coordinates returns valid coordinates"""
    test_coords = get_mid_dish_coordinates("SKA001")
    assert (test_coords.x.value - (5109058.063)) < TOLERANCE
    assert (test_coords.y.value - (2007302.436)) < TOLERANCE
    assert (test_coords.z.value - (-3239167.0)) < TOLERANCE

    with pytest.raises(RuntimeError):
        get_low_station_coordinates("B0")


def test_subarray_template_library():
    """
    Test if creating a subarray template using SubarrayTemplateLibrary() returns the
    correct telescope-specific object
    """
    # Test Mid
    assert (
        type(SubarrayTemplateLibrary().get_subarray_template("MID_FULL_AA4"))
        == MidSubArray
    )
    assert (
        type(SubarrayTemplateLibrary().get_subarray_template("Mid_15M_AA4"))
        == MidSubArray
    )
    # Test Low
    assert (
        type(SubarrayTemplateLibrary().get_subarray_template("LOW_FULL_AA4"))
        == LowSubArray
    )
    assert (
        type(SubarrayTemplateLibrary().get_subarray_template("loW_FULL_AA4"))
        == LowSubArray
    )
    # Test if AssertionError is thrown if a wrong template name is specified
    with pytest.raises(AssertionError):
        SubarrayTemplateLibrary().get_subarray_template("MID_FULL_AA5")


def test_subarray_template_library_substations():
    """
    Test if creating a subarray template containing substations using
    SubarrayTemplateLibrary() returns the correct telescope-specific object
    """
    library = SubarrayTemplateLibrary()
    assert set(library.valid_templates_substations) == {
        "LOW_SUBSTATION_18M_R1KM_AASTAR",
        "LOW_SUBSTATION_18M_R1KM_AA4",
    }
    assert (
        type(
            SubarrayTemplateLibrary().get_subarray_template(
                "low_substation_18m_r1km_aa4"
            )
        )
        == LowSubArray
    )


def test_n_antennas_in_AA():
    """Test if Mid and Low have correct number of antennas in each AA"""
    # Low AA0.5 has 6 stations
    assert len(LowSubArray(subarray_type="AA0.5").array_config.names.data) == 4
    # Low AA1 has 18 stations
    assert len(LowSubArray(subarray_type="AA1").array_config.names.data) == 18
    # Low AA2 has 64 stations
    assert len(LowSubArray(subarray_type="AA2").array_config.names.data) == 64
    # Low AA* has 307 stations
    assert len(LowSubArray(subarray_type="AA*").array_config.names.data) == 307
    # Low AA4 has 512 stations
    assert len(LowSubArray(subarray_type="AA4").array_config.names.data) == 512

    # Mid AA0.5 has 4 stations
    assert len(MidSubArray(subarray_type="AA0.5").array_config.names.data) == 4
    # Mid AA1 has 8 stations
    assert len(MidSubArray(subarray_type="AA1").array_config.names.data) == 8
    # Mid AA2 has 64 dishes
    assert len(MidSubArray(subarray_type="AA2").array_config.names.data) == 64
    # Mid AA* has 144 stations
    assert len(MidSubArray(subarray_type="AA*").array_config.names.data) == 144
    # Mid AA4 has 197 stations
    assert len(MidSubArray(subarray_type="AA4").array_config.names.data) == 197


def test_external_facility_failure():
    """Test that LowSubArray and MidSubArray classes fail if the specified external
    facility if of the wrong format"""
    extern_1 = ExternalTelescope(
        label="N16-1",
        location=EarthLocation.from_geodetic(
            lon=116.659076, lat=-26.497529, ellipsoid="WGS84"
        ),
        station_diameter=10.0,
    )

    with pytest.raises(TypeError):
        LowSubArray(
            subarray_type="custom", custom_stations="C*", external_telescopes=extern_1
        )

    with pytest.raises(TypeError):
        LowSubArray(
            subarray_type="custom",
            custom_stations="C*",
            external_telescopes=[extern_1.location],
        )


def test_low_external_telescope():
    """Test if specifying an external telescope works as intended"""
    # We will use a SKA LOW station as an external facility so that we can verify the outcome
    extern_1 = ExternalTelescope(
        label="N16-4",
        location=EarthLocation.from_geocentric(
            x=-2563011.7391289035 * units.m,
            y=5104950.875311578 * units.m,
            z=-2828669.846196786 * units.m,
        ),
        station_diameter=10.0,
    )

    low_test = LowSubArray(
        subarray_type="custom",
        custom_stations="N16-2,N16-3",
        external_telescopes=[extern_1],
    )

    low_custom = LowSubArray(
        subarray_type="custom", custom_stations="N16-2,N16-3,N16-4"
    )

    assert (
        numpy.absolute(
            low_custom.array_config.xyz.data - low_test.array_config.xyz.data
        )
        < TOLERANCE
    ).all()


def test_mid_external_telescope():
    """Test if specifying an external telescope works as intended"""
    # We will use a SKA MID station as an external facility so that we can verify the outcome
    extern_1 = ExternalTelescope(
        label="M002",
        location=EarthLocation.from_geocentric(
            x=5109272.092 * units.m,
            y=2006783.504 * units.m,
            z=-3239145.261 * units.m,
        ),
        station_diameter=10.0,
    )

    mid_test = MidSubArray(
        subarray_type="custom",
        custom_stations="M000,M001",
        external_telescopes=[extern_1],
    )

    mid_custom = MidSubArray(subarray_type="custom", custom_stations="M000,M001,M002")

    assert (
        numpy.absolute(
            mid_custom.array_config.xyz.data - mid_test.array_config.xyz.data
        )
        < TOLERANCE
    ).all()


def test_low_custom_layout():
    """Test the LOW custom layout"""
    low = LowSubArray(subarray_type="custom", custom_stations="E1-*,C66")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_custom.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_exclude_stations_failure():
    """Test for appropriate exclude_stations parameter in LowSubArray class"""
    with pytest.raises(ValueError):
        LowSubArray(subarray_type="AA0.5", exclude_stations="*")


def test_low_exclude_stations_not_in_subarray():
    """Test user selected excluded station is included in subarray"""
    with pytest.raises(UserWarning):
        warnings.simplefilter("error")
        LowSubArray(subarray_type="AA0.5", exclude_stations="S16-1")
        warnings.resetwarnings()


def test_low_exclude_stations_successful_template():
    """Test case for successful usage of exclude_stations in LOW"""
    low = LowSubArray(subarray_type="AA0.5", exclude_stations="S8-1")
    valid_list = "S8-6,S9-2,S10-3"
    assert sorted(low.array_config.names.data.tolist()) == sorted(valid_list.split(","))


def test_low_aa05_layout():
    """Test the LOW AA0.5 layout"""
    low = LowSubArray(subarray_type="AA0.5")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa05.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa1_layout():
    """Test the LOW AA1 layout"""
    low = LowSubArray(subarray_type="AA1")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa1.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa2_layout():
    """Test the LOW AA2 layout"""
    low = LowSubArray(subarray_type="AA2")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa2.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aastar_layout():
    """Test the LOW AA* layout"""
    low = LowSubArray(subarray_type="AA*")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aastar.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_low_aa4_layout():
    """Test the LOW AA4 layout"""
    low = LowSubArray(subarray_type="AA4")
    pickle_file = Path(__file__).resolve().parent.parent / "static/low_aa4.pickle"
    with open(pickle_file, "rb") as f:
        expected_low = pickle.load(f)
    assert (low.array_config.names == expected_low.array_config.names).all()
    assert (
        low.array_config.xyz.data - expected_low.array_config.xyz.data < TOLERANCE
    ).all()
    assert (low.array_config.diameter == expected_low.array_config.diameter).all()
    assert (low.array_config.mount == expected_low.array_config.mount).all()
    assert (low.array_config.vp_type == expected_low.array_config.vp_type).all()
    assert (low.array_config.location.x == expected_low.array_config.location.x).all()
    assert (low.array_config.location.y == expected_low.array_config.location.y).all()
    assert (low.array_config.location.z == expected_low.array_config.location.z).all()


def test_mid_custom_layout():
    """Test the MID custom layout"""
    mid = MidSubArray(subarray_type="custom", custom_stations="M*,SKA001")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_custom.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa05_layout():
    """Test the MID AA0.5 layout"""
    mid = MidSubArray(subarray_type="AA0.5")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa05.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa1_layout():
    """Test the MID AA1 layout"""
    mid = MidSubArray(subarray_type="AA1")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa1.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa2_layout():
    """Test the MID AA2 layout"""
    mid = MidSubArray(subarray_type="AA2")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa2.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aastar_layout():
    """Test the MID AA* layout"""
    mid = MidSubArray(subarray_type="AA*")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aastar.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_aa4_layout():
    """Test the MID AA4 layout"""
    mid = MidSubArray(subarray_type="AA4")
    pickle_file = Path(__file__).resolve().parent.parent / "static/mid_aa4.pickle"
    with open(pickle_file, "rb") as f:
        expected_mid = pickle.load(f)
    assert (mid.array_config.names == expected_mid.array_config.names).all()
    assert (
        mid.array_config.xyz.data - expected_mid.array_config.xyz.data < TOLERANCE
    ).all()
    assert (mid.array_config.diameter == expected_mid.array_config.diameter).all()
    assert (mid.array_config.mount == expected_mid.array_config.mount).all()
    assert (mid.array_config.vp_type == expected_mid.array_config.vp_type).all()
    assert (mid.array_config.location.x == expected_mid.array_config.location.x).all()
    assert (mid.array_config.location.y == expected_mid.array_config.location.y).all()
    assert (mid.array_config.location.z == expected_mid.array_config.location.z).all()


def test_mid_invalid_custom_string():
    """Test the MID custom string"""
    with pytest.raises(ValueError) as err:
        MidSubArray(subarray_type="custom", custom_stations="C*")


def test_mid_empty_subarray():
    """Test for appropriate exclude_stations parameter in MidSubArray class"""
    with pytest.raises(ValueError):
        MidSubArray(subarray_type="AA0.5", exclude_stations="*")


def test_mid_exclude_stations_not_in_subarray():
    """Test user selected excluded station is included in MID subarray"""
    with pytest.raises(UserWarning):
        warnings.simplefilter("error")
        MidSubArray(subarray_type="AA0.5", exclude_stations="M000")
        warnings.resetwarnings()


def test_mid_exclude_stations_successful_template():
    """Test case for successful usage of exclude_stations in MID"""
    mid_arr = MidSubArray(subarray_type="AA0.5", exclude_stations="SKA001")
    valid_list = "SKA036,SKA063,SKA100"
    assert sorted(mid_arr.array_config.names.data.tolist()) == sorted(
        valid_list.split(",")
    )


def test_low_invalid_custom_string():
    """Test the LOW custom string"""
    with pytest.raises(ValueError) as err:
        LowSubArray(subarray_type="custom", custom_stations="M*")


def test_low_invalid_subarray_type():
    """Test the low subarray type"""
    with pytest.raises(AssertionError) as err:
        LowSubArray(subarray_type="error_string")


def test_mid_invalid_subarray_type():
    """Test the low subarray type"""
    with pytest.raises(AssertionError) as err:
        LowSubArray(subarray_type="error_string")


def test_array_layout(test_image_name):
    low = LowSubArray(subarray_type="AA*")
    fig, _ = low.plot_array_layout(scale="")
    fig.savefig(test_image_name)
    reference_image = (
        Path(__file__).resolve().parent.parent / "static/low_reference_array_layout.png"
    )
    assert compare_images(test_image_name, reference_image, tol=TOLERANCE) is None


def test_low_casa_file(test_casa_ant_name):
    low_custom = LowSubArray(subarray_type="custom", custom_stations="C*,E1-*")
    low_custom.generate_casa_antenna_list(test_casa_ant_name)
    reference_file = (
        Path(__file__).resolve().parent.parent / "static/low_reference.casa.cfg"
    )
    assert filecmp.cmp(test_casa_ant_name, reference_file, shallow=False)


def test_mid_casa_file(test_casa_ant_name):
    mid_custom = MidSubArray(subarray_type="custom", custom_stations="M*,SKA017")
    mid_custom.generate_casa_antenna_list(test_casa_ant_name)
    reference_file = (
        Path(__file__).resolve().parent.parent / "static/mid_reference.casa.cfg"
    )
    assert filecmp.cmp(test_casa_ant_name, reference_file, shallow=False)


def test_filter_array_by_distance_float():
    mid_aastar = MidSubArray(subarray_type="AA*")
    expected_result = sorted(
        (
            "M000,M001,M002,M003,M004,M005,M006,M018,M029,SKA049,SKA050,SKA051,SKA075,"
            + "SKA079,SKA081,SKA082"
        ).split(",")
    )

    mid_core_r125 = filter_array_by_distance(mid_aastar, distance=125.0)
    assert expected_result == sorted(mid_core_r125.split(","))


def test_filter_array_by_distance_units():
    mid_aastar = MidSubArray(subarray_type="AA*")
    expected_result = sorted(
        (
            "M000,M001,M002,M003,M004,M005,M006,M018,M029,SKA049,SKA050,SKA051,SKA075,"
            "SKA079,SKA081,SKA082"
        ).split(",")
    )
    mid_core_r125 = filter_array_by_distance(mid_aastar, distance=125.0 * units.m)
    assert expected_result == sorted(mid_core_r125.split(","))

    # Test for failure case when the specified unit is wrong
    with pytest.raises(ValueError):
        filter_array_by_distance(mid_aastar, distance=125.0 * units.second)
